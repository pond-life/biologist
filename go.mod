module git.sr.ht/~hokiegeek/biologist

go 1.12

require (
	git.sr.ht/~hokiegeek/life v3.0.0+incompatible
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
)
