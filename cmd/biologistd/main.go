package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"git.sr.ht/~hokiegeek/biologist"
	"git.sr.ht/~hokiegeek/life"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

/////////////////////////////////// CREATE ANALYSIS ///////////////////////////////////

// CreateAnalysisResponse encapsulates the HTTP response for a CreateAnalysisRequest
type CreateAnalysisResponse struct { // {{{
	ID   []byte
	Dims life.Dimensions
} // }}}

type patternType int // {{{

const (
	// USER specifies that the pattern was passed in by the user in the request
	USER patternType = iota
	// RANDOM specifies that a random pattern should be generated
	RANDOM
) // }}}

// CreateAnalysisRequest encapsulates the needed initial data for starting a life simulation
type CreateAnalysisRequest struct { // {{{
	Dims    life.Dimensions
	pattern patternType
	Seed    []life.Location
	// life.Rules
}

func (t *CreateAnalysisRequest) String() string {
	var buf bytes.Buffer

	buf.WriteString(t.Dims.String())

	return buf.String()
}

func createAnalysis(mgr *biologist.Manager, log *log.Logger, w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("could not process payload: %s\n", err)
		http.Error(w, "could not process payload", http.StatusBadRequest)
		return
	}

	var req CreateAnalysisRequest
	if err := json.Unmarshal(body, &req); err != nil {
		log.Printf("error: could not handle request: %v\n", err)
		log.Printf("error: request: %s\n", body)
		http.Error(w, "could not process payload", http.StatusUnprocessableEntity)
		return
	}

	log.Printf(":: Received create request: %s\n", body)

	// Determine the pattern to use for seeding the board
	var patternFunc func(life.Dimensions, life.Location) []life.Location
	switch req.pattern {
	case USER:
		patternFunc = func(dims life.Dimensions, offset life.Location) []life.Location {
			return req.Seed
		}
	case RANDOM:
		patternFunc = func(dims life.Dimensions, offset life.Location) []life.Location {
			return life.Random(dims, offset, 35)
		}
	}

	// Create the biologist
	biologist, err := biologist.New(req.Dims, patternFunc, life.ConwayTester())
	if err != nil {
		log.Printf("error: could not create biologist: %v\n", err)
		http.Error(w, "could not process payload", http.StatusInternalServerError)
		return
	}

	// Respond the request with the ID of the biologist
	mgr.Add(biologist)
	postJSON(w, http.StatusCreated, CreateAnalysisResponse{biologist.ID, biologist.Life.Dimensions()})
} // }}}

/////////////////////////////////// UPDATE ANALYSIS ///////////////////////////////////

// AnalysisUpdate encapsulates the analysis of a given generation
type AnalysisUpdate struct { // {{{
	ID         []byte
	Dims       life.Dimensions
	Status     string
	Generation int
	Living     []life.Location
	// Changes    []biologist.ChangedLocation
}

func newAnalysisUpdate(biologist *biologist.Biologist, generation int) *AnalysisUpdate {
	analysis := biologist.Analysis(generation)
	if analysis == nil {
		return nil
	}

	a := new(AnalysisUpdate)

	a.ID = biologist.ID
	a.Dims = biologist.Life.Dimensions()
	a.Generation = generation

	a.Status = analysis.Status.String()

	a.Living = make([]life.Location, len(analysis.Living))
	copy(a.Living, analysis.Living)

	// a.Changes = make([]biologist.ChangedLocation, len(analysis.Changes))
	// copy(a.Changes, analysis.Changes)

	return a
} // }}}

// AnalysisUpdateRequest encapsulates required parameters to retrieve the analysis of a range of generations
type AnalysisUpdateRequest struct { // {{{
	ID                 []byte
	StartingGeneration int
	NumMaxGenerations  int
}

func (t *AnalysisUpdateRequest) String() string {
	var buf bytes.Buffer

	buf.WriteString("ID: ")
	buf.WriteString(fmt.Sprintf("%x", t.ID))
	buf.WriteString("\nStarting Generation: ")
	buf.WriteString(strconv.Itoa(t.StartingGeneration))
	buf.WriteString("\nMax: ")
	buf.WriteString(strconv.Itoa(t.NumMaxGenerations))

	return buf.String()
} // }}}

// AnalysisUpdateResponse encapsulates the requested analysis updates
type AnalysisUpdateResponse struct { // {{{
	ID      []byte
	Updates []AnalysisUpdate
}

func newAnalysisUpdateResponse(log *log.Logger, biologist *biologist.Biologist, startingGeneration int, maxGenerations int) (r AnalysisUpdateResponse, err error) {
	// fmt.Printf("NewAnalysisUpdateResponse(%d, %d)\n", startingGeneration, maxGenerations)
	if biologist == nil {
		return r, fmt.Errorf("biologist not found")
	}

	r.ID = biologist.ID
	r.Updates = make([]AnalysisUpdate, 0)

	// Retrieve as many updates as are available up to the max
	endGen := startingGeneration + maxGenerations
	for i := startingGeneration; i < endGen; i++ {
		update := newAnalysisUpdate(biologist, i)
		if update != nil {
			r.Updates = append(r.Updates, *update)
		}
	}
	log.Printf("Sending %d updates starting at generation %d\n", len(r.Updates), startingGeneration)

	return
} // }}}

func getAnalysisStatus(mgr *biologist.Manager, log *log.Logger, w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("could not process payload: %s\n", err)
		http.Error(w, "could not process payload", http.StatusBadRequest)
		return
	}

	var req AnalysisUpdateRequest
	if err := json.Unmarshal(body, &req); err != nil {
		log.Printf("error: could not handle request: %v\n", err)
		log.Printf("error: request: %s\n", req.String())
		http.Error(w, "could not process payload", http.StatusUnprocessableEntity)
		return
	}

	log.Printf(":: Received poll request: %s\n", body)

	resp, err := newAnalysisUpdateResponse(log, mgr.Biologist(req.ID), req.StartingGeneration, req.NumMaxGenerations)
	if err != nil {
		log.Printf("error: could not build analysis response: %s\n", req.String())
		http.Error(w, "could not analyze response", http.StatusInternalServerError)
		return
	}

	postJSON(w, http.StatusCreated, resp)
}

/////////////////////////////////// CONTROL ANALYSIS ///////////////////////////////////

// ControlOrder enumerates the available commands to control a running simulation/analysis
type ControlOrder int // {{{

const (
	// Start will begin the simulation and subsequent analysis
	Start ControlOrder = 0
	// Stop will stop the simulation (and analysis)
	Stop ControlOrder = 1
	// Remove will get rid of the simulation completely
	Remove ControlOrder = 2
)

// ControlRequest encapsulates the HTTP request for controlling a simulation
type ControlRequest struct {
	ID    []byte
	Order ControlOrder
}

func (t *ControlRequest) String() string {
	var buf bytes.Buffer
	buf.WriteString("ID: ")
	buf.WriteString(fmt.Sprintf("%x", t.ID))
	buf.WriteString("\nOrder: ")
	switch t.Order {
	case 0:
		buf.WriteString("Start")
	case 1:
		buf.WriteString("Stop")
	case 2:
		buf.WriteString("Remove")
	}

	return buf.String()
} // }}}

func controlAnalysis(mgr *biologist.Manager, log *log.Logger, w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		return
	}

	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("could not process payload: %s\n", err)
		http.Error(w, "could not process payload", http.StatusBadRequest)
		return
	}

	var req ControlRequest

	if err := json.Unmarshal(body, &req); err != nil {
		log.Printf("error: could not handle request: %v\n", err)
		log.Printf("error: request: %s\n", req.String())
		http.Error(w, "could not process payload", http.StatusUnprocessableEntity)
		return
	}

	log.Printf(":: Received control request: %s\n", body)

	biologist := mgr.Biologist(req.ID)

	switch req.Order {
	case Start:
		biologist.Start()
	case Stop:
		biologist.Stop()
	case Remove:
		biologist.Stop()
		mgr.Remove(req.ID)
	}
}

/////////////////////////////////// OTHER ///////////////////////////////////

func postJSON(w http.ResponseWriter, httpStatus int, send interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	if err := json.NewEncoder(w).Encode(send); err != nil {
		http.Error(w, "could not encode payload", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(httpStatus)
}

func main() {
	logger := log.New(os.Stdout, "[biologistd] ", 0)
	portPtr := flag.String("port", "8080", "Specify the port to use")
	flag.Parse()

	port := os.Getenv("PORT")
	if port == "" {
		port = *portPtr
	}

	mgr := biologist.NewManager()

	mux := mux.NewRouter()
	mux.HandleFunc("/analyze",
		func(w http.ResponseWriter, r *http.Request) {
			createAnalysis(mgr, logger, w, r)
		}).Methods("HEAD", "POST", "OPTIONS")
	mux.HandleFunc("/poll",
		func(w http.ResponseWriter, r *http.Request) {
			getAnalysisStatus(mgr, logger, w, r)
		}).Methods("HEAD", "GET", "OPTIONS", "POST")
	mux.HandleFunc("/control",
		func(w http.ResponseWriter, r *http.Request) {
			controlAnalysis(mgr, logger, w, r)
		}).Methods("HEAD", "POST", "OPTIONS")
	mux.PathPrefix("/").Handler(http.FileServer(http.Dir("/web")))

	originsOk := handlers.AllowedOrigins([]string{"*"})
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization", "If-None-Match"})
	exposedOk := handlers.ExposedHeaders([]string{"Content-Type", "Content-Length", "Accept-Encoding", "Authorization", "Etag"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	log.Printf("Started server on port %s\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), handlers.CORS(originsOk, headersOk, methodsOk, exposedOk)(mux)))
}

// vim: set foldmethod=marker:
